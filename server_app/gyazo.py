#!/usr/bin/env python
from hashlib import md5
from time import time
import logging
import webapp2
from google.appengine.api import app_identity, images
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from lib import cloudstorage

GCS_BUCKET = app_identity.get_default_gcs_bucket_name()

class CloudStoreUtils:

    @staticmethod
    def get_bucket_url(prefix=None, postfix=None):
        if prefix:
            prefix = '/' + prefix
        else:
            prefix = ''
        if not postfix:
            postfix = ''
        return '{1:s}/{0:s}/images/{2:s}'.format(GCS_BUCKET, prefix, postfix)

    @staticmethod
    def get_bucket_contents():
        bucket_url = CloudStoreUtils.get_bucket_url()
        return cloudstorage.listbucket(bucket_url)

    @staticmethod
    def is_file_available(file_name, bucket_content):
        logging.debug('> Checking if file "{}" is available.'.format(file_name))
        is_available = False
        for item in bucket_content:
            path_lst = item.filename.split('/')
            if file_name.split('.')[0] in item.filename:
                logging.debug('> Similar file found. "{}", "{}"'.format(file_name, item.filename))
            if path_lst[len(path_lst) - 1] == file_name:
                is_available = True
                logging.info('> File "{}" is found in the file list.'.format(file_name))
                break
        else:
            logging.info('> File "{}" is not found in the file list.'.format(file_name))
        return is_available

class KeepAliveHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write("I'm not dead!")
        logging.debug('Keep-alive initiated!')

class StalkHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write(
            '''
                <!DOCTYPE html>
                <html>
                <head>Stalk Alert!!!</head>
                <body>
                    <h1>Dude! Don't stalk me!!</h1>
                    <p>You can't directly access this site unless you are the <a href="{:s}">admin</a>.</p>
                </body>
                </html>
            '''.format('/admin')
        )
        logging.info('Stalker homepage loaded!')

class ImageServeHandler(blobstore_handlers.BlobstoreDownloadHandler):

    def get(self, file_ident):
        logging.info('The request filename is {}'.format(file_ident))
        full_file_name = file_ident.lower()
        request_file_name = full_file_name.rsplit('.', 1)
        file_name = request_file_name[0]
        file_ext = None if len(request_file_name) < 2 else request_file_name[1]
        bucket_iter = CloudStoreUtils.get_bucket_contents()
        # Making item list with possible files/filepaths.
        bucket_content = [item for item in bucket_iter if file_name in item.filename]
        #Check for full file name
        if not CloudStoreUtils.is_file_available(full_file_name, bucket_content):
            if file_ext not in ['jpg', 'jpeg']:
                #Check for file name + png
                png_file_name = full_file_name + '.png'
                if not CloudStoreUtils.is_file_available(png_file_name, bucket_content):
                    logging.info('No file named "{}" or "{}"'.format(
                        full_file_name, png_file_name))
                    self.response.write('No blob')  # No file with file fullname + png(and fn only)
                else:
                    logging.info('Serving file "{}"'.format(png_file_name))
                    self.response.headers["Cache-Control"] = "public, max-age=86400" # Cache for 1 day
                    self.serve_file(png_file_name)
            else:
                #Convert image to jpeg and send
                png_file_name = file_name + '.png'
                logging.debug('*.jpg in filename "{}", but jpg not found.'.format(full_file_name))
                logging.info('Searching for "{}" to convert into jpg.'.format(png_file_name))
                if not CloudStoreUtils.is_file_available(png_file_name, bucket_content):
                    logging.info('No file named "{}" to convert into a jpg.'.format(png_file_name))
                    self.response.write('No blob')  # No file with file filename + png
                else:
                    blob_key = self.get_blob_key_from_filename(png_file_name)
                    img = images.Image(blob_key=blob_key)
                    img.rotate(0)
                    jpg_img = img.execute_transforms(output_encoding=images.JPEG, quality=92)
                    logging.info('Serving converted jpg file "{}" from "{}"'.format(
                        full_file_name, png_file_name))
                    logging.debug(blob_key)
                    self.response.headers["Cache-Control"] = "public, max-age=86400" # Cache for 1 day
                    self.response.headers['Content-Type'] = 'image/jpeg'
                    self.response.write(jpg_img)
        else:
            logging.info('File found. Serving file "{}"'.format(full_file_name))
            self.serve_file(full_file_name)

    def serve_file(self, file_name):
        blob_key = self.get_blob_key_from_filename(file_name)
        self.response.headers["Cache-Control"] = "public, max-age=86400"  # Cache for 1 day
        logging.info('>> Serving file "{}" from blobstore.'.format(file_name))
        logging.debug('File blob key = ' + blob_key)
        self.send_blob(blob_key)

    def get_blob_key_from_filename(self, file_name):
        gs_file = CloudStoreUtils.get_bucket_url(prefix='gs', postfix=file_name)
        logging.info('GCS blobstore url = ' + gs_file)
        blob_key = blobstore.create_gs_key(gs_file)
        return blob_key

class UploadHandler(webapp2.RequestHandler):
    def post(self):
        try:
            image_info = self.request.POST['imagedata']
            type_ext = {'image/png': 'png', 'image/jpeg': 'jpg'}
            assert image_info.type in type_ext.keys()
            image_data = image_info.file
            content_length = self.request.headers['CONTENT_LENGTH']
            if content_length > 0:
                bucket_iter = CloudStoreUtils.get_bucket_contents()
                bucket_content = [item.filename for item in bucket_iter]
                l_count = 0
                while l_count < 100:
                    # Generate a new filename
                    l_count += 1
                    filename = md5(str(time())).hexdigest()[-7:]
                    if bucket_content:
                        for name in bucket_content:
                            if not filename in name:
                                break
                        else:
                            continue
                    break

                if l_count >= 100:
                    logging.error("Cannot create a unique filename after 100 tries.")
                    return
                new_filename = '{}.{}'.format(filename, type_ext[image_info.type])
                gcs_full_filename = CloudStoreUtils.get_bucket_url(postfix=new_filename)
                gcs_file = cloudstorage.open(
                    filename=gcs_full_filename,
                    mode='w', content_type=image_info.type)
                gcs_file.write(image_data.read())
                gcs_file.close()
                logging.info('New file created and saved as "{}".'.format(gcs_full_filename))
                return_url = self.request.host_url + '/' + new_filename
                self.response.write(return_url)
                logging.info('Returned url "{}"'.format(return_url))
        except Exception as e:
            logging.error('Exception occurred with message "{0}".'.format(e.message))

    def get(self):
        pass


app = webapp2.WSGIApplication([
    (r'/', StalkHandler),
    (r'/alive', KeepAliveHandler),
    (r'/upload', UploadHandler),
    (r'/([^/]+).*', ImageServeHandler)
], debug=True)
