from __future__ import print_function
import requests
import os
import pytest

URL = 'http://localhost:8080/upload'


class HelperMethods:
    @staticmethod
    def send_file(file_path, image_mime, file_name='data.png'):
        req = requests.post(
            URL,
            files={'imagedata': (file_name, open(file_path, 'rb'), image_mime)},
            data={'id': 'a2cc42a4a33d93d92ef4a44b840ce51a'})  # magic value
        return req

    @staticmethod
    def request_file(url):
        req = requests.get(url)
        return req

class TestModule:

    @classmethod
    def setup_class(cls):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))

    def test_send_small_png(self):
        filename = 'files/AAAAWWYYYEEAAAAHHHH.png'
        mime = 'image/png'
        req = HelperMethods.send_file(filename, mime)
        returned_url = req.content
        print(returned_url)
        get_img_req = HelperMethods.request_file(returned_url)
        assert get_img_req.headers['content-type'] == mime
        assert len(get_img_req.content) == os.path.getsize(filename)

    def test_send_png(self):
        filename = 'files/test_png.png'
        mime = 'image/png'
        req = HelperMethods.send_file(filename, mime)
        returned_url = req.content
        print(returned_url)
        get_img_req = HelperMethods.request_file(returned_url)
        assert get_img_req.headers['content-type'] == mime
        assert len(get_img_req.content) == os.path.getsize(filename)

    def test_send_jpg(self):
        filename = 'files/test_jpg.jpg'
        mime = 'image/jpeg'
        req = HelperMethods.send_file(filename, mime)
        returned_url = req.content
        print(returned_url)
        get_img_req = HelperMethods.request_file(returned_url)
        assert get_img_req.headers['content-type'] == mime
        assert len(get_img_req.content) == os.path.getsize(filename)

    def test_jpeg_conversion(self):
        filename = 'files/test_png.png'
        mime = 'image/png'
        req = HelperMethods.send_file(filename, mime)
        returned_url = req.content
        print(returned_url)
        jpeg_url = returned_url.rsplit('.', 1)[0] + '.jpg'
        get_img_req = HelperMethods.request_file(jpeg_url)
        assert get_img_req.headers['content-type'] == 'image/jpeg'
        assert len(get_img_req.content) != os.path.getsize(filename)
